from django.db import models

# Create your models here.
class Dog(models.Model):
    name = models.CharField(max_length=200)
    picture = models.URLField()
    weight = models.PositiveSmallIntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
