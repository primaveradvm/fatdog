from django.shortcuts import render, get_object_or_404
from dogs.models import Dog

# Create your views here.
def dogs(request):
    dogs = Dog.objects.all()
    context = {
        "dogs": dogs
    }
    return render(request, "dogs/list.html", context)
