from django.urls import path
from dogs.views import dogs

urlpatterns =[
    path("", dogs, name="dogs"),
]
