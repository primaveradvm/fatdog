from django.contrib import admin
from dogs.models import Dog

# Register your models here.
@admin.register(Dog)
class DogAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]
